const express = require('express');
const router = express.Router();


const Home = require('./Home/index');

router.use('/home', Home);

module.exports = router;