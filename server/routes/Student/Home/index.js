const express = require('express');
const router = express.Router();
const multer = require('multer');//upload document
const path = require('path');
const fs = require('fs');
const config = require('../../../config');
const mongo = require('../../../mongo');
const MD5 = require('crypto-js/md5');//secure hash funtions Its use for password
const imageStorage = multer.diskStorage({
  // Destination to store image     
  destination: 'uploads/',
  destination: async function (req, file, cb) {
    let path = 'uploads/' + req.body._id;
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path, { recursive: true }, (err) => {
        if (err) throw err;
      });
    }
    await cb(null, path);
  },
  filename: (req, file, cb) => {
    cb(null, req.body.filename + path.extname(file.originalname))
  }
});

const imageUpload = multer({
  storage: imageStorage,
  limits: {
    fileSize: 1000000 // 1000000 Bytes = 1 MB
  },
  fileFilter(req, file, cb) {
    if (!file.originalname.match(/\.(pdf)$/)) {
      return cb(new Error('Please upload a PDF'))
    }
    cb(undefined, true);
  }
})

router.post('/', imageUpload.single('file'), async (req, res) => {

  let id = parseFloat(req.body._id);
  let type = req.body.filename;
  let query = {};
  let x = `status.${type}`;
  query[x] = 0;
  let db1 = mongo.get().collection("user_table")
  let accept = await db1.updateOne({ "_id": id }, { $set: query })
    .catch(err => res.json({ "Status": -1, msg: "Error. " }));

  let loginUser = await db1.find({ "_id": id }).toArray()
    .catch(err => res.json({ "Status": -1, msg: "Login Error. " }));

  res.json({ "Status": 1, "userdetail": loginUser });
 
}, (error, req, res, next) => {
  res.status(400).send({ error: error.message })

});


router.post('/getstatus', async (req, res) => {
  let _id = parseFloat(req.body._id);
  let status = {};
  let filenames = req.body.filenames;
  for (let i = 0; i < filenames.length; i++) {
    let path = "uploads/" + _id + "/" + filenames[i] + ".pdf";
    status[filenames[i]] = fs.existsSync(path);
  }

  res.json({ Status: 1, statusInfo: status });
});


router.post('/uploadstatus', async (req, res) => {

  let db1 = mongo.get().collection("user_table")

  loginUser = await db1.find({ "_id": parseFloat(req.body.id), "status.sslc": 1, "status.hsc": 1, "status.tc": 1, "status.tn-marksheet": 1, "status.tn-hallticket": 1, "status.cc": 1, "status.sign": 1, "status.sem1": 1, "status.sem2": 1, "status.sem3": 1, "status.sem4": 1, "status.sem5": 1, "status.sem6": 1, "status.add-certificate1": 1, "status.add-certificate2": 1, "status.degree": 1 }).toArray()
    .catch(err => res.json({ "Status": -1, msg: "Login Error. " }));
  if (loginUser.length > 0) {
    res.json({ status: 1 })
  }
  else {
    res.json({ status: 0 })
  }
});


router.post('/searchdata', async (req, res) => {
  let db1 = mongo.get().collection("user_table")
  let loginUser = await db1.find({ "_id": req.body._id }).toArray()
    .catch(err => res.json({ "Status": -1, msg: "Login Error. " }));
  if (loginUser.length > 0) {
    res.json({ "Status": 1, user_details: loginUser });
  }
  else {
    res.json({ "Status": 0 });
  }
});

router.post('/profile', async (req, res) => {


  let db1 = mongo.get().collection("user_table")
  let id = parseFloat(req.body.id);
  let oldpassword = MD5(req.body.oldpassword).toString();
  let checkoldpassword = await db1.find({ "_id": id, "password": oldpassword}).toArray()
 
  
  if (checkoldpassword.length) {
    let query = {
      "first_name": req.body.firstname.toUpperCase(),
      "last_name": req.body.lastname.toUpperCase(),
      "ph_no": req.body.phonenumber,
      "email": req.body.email,
      "password": MD5(req.body.confirmpassword).toString(),
      "degree": req.body.degree.toUpperCase(),
    }

    let accept = await db1.updateOne({ "_id": id }, { $set: query })
      .catch(err => res.json({ "Status": -1, msg: "Error. " }));
   accept.acknowledged ? res.json({ "Status": 1 }) : res.json({ "Status": 0 });
  }
  else {
    res.json({ "Status": -1 });
  }
});

router.post('/rank', async (req, res) => {
  let db1 = mongo.get().collection("user_table")

  let rank = await db1.find({"role": "student"}).sort({tancet_mark: -1 }).toArray()
console.log(rank)
  
  res.json({ status: rank });
});

router.post('/bc', async (req, res) => {
  let db1 = mongo.get().collection("user_table")

  let rank = await db1.find({ community:"BC" }).sort({ tancet_mark: -1 }).toArray()
  console.log(rank)

  res.json({ status: rank });
});

router.post('/mbc', async (req, res) => {
  let db1 = mongo.get().collection("user_table")

  let rank = await db1.find({ community: "MBC" }).sort({ tancet_mark: -1 }).toArray()
  console.log(rank)

  res.json({ status: rank });
});


router.post('/sc', async (req, res) => {
  let db1 = mongo.get().collection("user_table")

  let rank = await db1.find({ community: "SC" }).sort({ tancet_mark: -1 }).toArray()
  console.log(rank)

  res.json({ status: rank });
});




module.exports = router;