const express = require('express');
const router = express.Router();

const Student = require('./Student');
const Login = require('./Login');
const Admin = require('./Admin/index');

router.use('/login', Login);
router.use('/student', Student);
router.use('/admin', Admin);

module.exports = router;