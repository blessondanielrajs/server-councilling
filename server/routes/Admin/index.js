const express = require('express');
const router = express.Router();
const MD5 = require('crypto-js/md5');//secure hash funtions Its use for password
const path = require('path');
const fs = require('fs');
const Bulk = require('./Bulk/index');
const mongo = require('../../mongo');
router.use('/bulk', Bulk);
router.post('/user', async (req, res) => {
    let db1 = mongo.get().collection("user_table")
    let loginUser = await db1.find({ "role": "student" }).toArray()
        .catch(err => res.json({ "Status": -1, msg: "Login Error. " }));
    if (loginUser.length > 0) {
        res.json({ "Status": 1, user_details: loginUser });
    }
    else {
        res.json({ "Status": 0 });
    }
});


router.post('/searchstudent', async (req, res) => {
    let db1 = mongo.get().collection("user_table")
    let status = {};
    let filenames = req.body.filenames;
    let searchstudent = await db1.find({ "_id": parseFloat(req.body.searchstudent) }).toArray()
        .catch(err => res.json({ "Status": -1, msg: "Login Error. " }));

    if (searchstudent.length > 0) {
        for (let i = 0; i < filenames.length; i++) {
            let path = "uploads/" + parseFloat(req.body.searchstudent) + "/" + filenames[i] + ".pdf";
            status[filenames[i]] = fs.existsSync(path);
        }

        res.json({ "Status": 1, user_details: searchstudent, statusInfo: status });

    }
    else {
        res.json({ "Status": 0 });
    }
});



router.post('/accept', async (req, res) => {
    let id = req.body.id;
    let type = req.body.branch;
    let query = {};
    let x = `status.${type}`;
    query[x] = 1;


    let db1 = mongo.get().collection("user_table")

    let accept = await db1.updateOne({ "_id": id }, { $set: query })
        .catch(err => res.json({ "Status": -1, msg: "Error. " }));


    let loginUser = await db1.find({ "_id": id }).toArray()
        .catch(err => res.json({ "Status": -1, msg: "Login Error. " }));

    if (accept.acknowledged) {
        res.json({ "Status": 1, "userdetail": loginUser });
    }
    else {

    }

});



router.post('/reject', async (req, res) => {
    let id = req.body.id;
    let type = req.body.branch;
    let query = {};
    let x = `status.${type}`;
    query[x] = -1;
    let db1 = mongo.get().collection("user_table")

    let accept = await db1.updateOne({ "_id": id }, { $set: query })
        .catch(err => res.json({ "Status": -1, msg: "Error. " }));

    let loginUser = await db1.find({ "_id": id }).toArray()
        .catch(err => res.json({ "Status": -1, msg: "Login Error. " }));
    if (accept.acknowledged) {

        res.json({ Status: 1, "userdetail": loginUser });
    }
    else {
        res.json({ Status: -1 });
    }
});

router.post('/searchreport', async (req, res) => {
    let type = req.body.topic;

    let status = parseInt(req.body.status);
    let query = { role: 'student' };
    let x = `status.${type}`;
    query[x] = status;
    let db1 = mongo.get().collection("user_table")
    let searchstudent = await db1.find(query).toArray()
        .catch(err => res.json({ "Status": -1, msg: "Login Error. " }));
    if (searchstudent.length > 0) {
        res.json({ Status: 1, studentdata: searchstudent });
    }
    else {
        res.json({ Status: -1, studentdata: [] });
    }
});


router.post('/statics', async (req, res) => {
    let db1 = mongo.get().collection("user_table")
    const type = ["sslc", "hsc", "degree", "tc", "tn-marksheet", "tn-hallticket", "cc", "sign", "photo", "sem1", "sem2", "sem3", "sem4", "sem5", "sem6", "add-certificate1", "add-certificate2"];
    let status = {};
    for (let i = 0; i < type.length; i++) {
        let query = {};
        let x = `status.${type[i]}`;
        query[x] = 0;
        status[type[i]] = await db1.find(query).count();
    }
    let registration = await db1.find({ "role": "admin" }).toArray()

    res.json({ status: status, checked: registration[0].registration });
});

router.post('/profile', async (req, res) => {

    let db1 = mongo.get().collection("user_table")
    let id = parseFloat(req.body.id);
    let checkoldpassword = await db1.find({ "_id": id, "password": req.body.oldpassword }).toArray()

    if (checkoldpassword.length) {
        let query = {
            "password": req.body.confirmPassword,
        }
        let accept = await db1.updateOne({ "_id": id }, { $set: query })
            .catch(err => res.json({ "Status": -1, msg: "Error. " }));
        accept.acknowledged ? res.json({ "Status": 1 }) : res.json({ "Status": 0 });
    }
    else {
        res.json({ "Status": -1 });
    }

});

router.post('/registation', async (req, res) => {
    let new_id = 101;
    let db1 = mongo.get().collection("user_table")
    let max = await db1.find({ "role": "STUDENT" }).sort({ _id: -1 }).limit(1).toArray()
    if (max.length !== 0)
        new_id = parseInt(max[0]._id) + 1;

    let json = {
        "_id": new_id,
        "key": new_id,
        "first_name": req.body.first_name.toUpperCase(),
        "last_name": req.body.last_name.toUpperCase(),
        "role": "student".toUpperCase(),
        "community": req.body.community.toUpperCase(),
        "ph_no": req.body.ph_no,
        "email": req.body.email,
        "appliction_number": req.body.appliction_number,
        "tancet_registration_number": req.body.tancet_registration_number,
        "tancet_mark": parseFloat(req.body.tancet_mark),
        "password": MD5(req.body.password).toString(),
        "degree": req.body.degree.toUpperCase(),
        "status": {
            "sslc": -2,
            "hsc": -2,
            "tc": -2,
            "tn-marksheet": -2,
            "tn-hallticket": -2,
            "cc": -2,
            "sign": -2,
            "photo": -2,
            "sem1": -2,
            "sem2": -2,
            "sem3": -2,
            "sem4": -2,
            "sem5": -2,
            "sem6": -2,
            "add-certificate1": -2,
            "add-certificate2": -2
        }
    }

    let insert = await db1.insert(json);

    if (insert.acknowledged) {
        res.json({ status: 1 });

    }
    else {
        res.json({ status: 0 });
    }
});


router.post('/checkedlogin', async (req, res) => {
    console.log(req.body)
    let db1 = mongo.get().collection("user_table")
    let query = { "registration": req.body.checked }
    let checked = await db1.updateOne({ "role": "admin" }, { $set: query })
        .catch(err => res.json({ "Status": -1, msg: "Error. " }));
    if (checked.acknowledged) {
        console.log("Done", query, checked)
    }
});

module.exports = router;