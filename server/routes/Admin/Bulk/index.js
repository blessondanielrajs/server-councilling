const express = require('express');
const router = express.Router();
const multer = require('multer'); //Its use for uploading
const path = require('path');
const fs = require('fs');
const MD5 = require('crypto-js/md5');//secure hash funtions Its use for password
var parse = require('csv-parse');// Excel format Use for bulk uploading
const config = require('../../../config');
const mongo = require('../../../mongo');
//image storage(documents)
const imageStorage = multer.diskStorage({
    destination: 'temp/',
    destination: async function (req, file, cb) {
        let path = 'temp/';
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path, { recursive: true }, (err) => {
                if (err) throw err;
            });
        }
        await cb(null, path);
    },
    filename: (req, file, cb) => {
        cb(null, "file" + path.extname(file.originalname))
    }
});

const imageUpload = multer({
    storage: imageStorage,
    limits: {
        fileSize: 1000000 // 1000000 Bytes = 1 MB
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(csv)$/)) {
            return cb(new Error('Please upload a CSV'))
        }
        cb(undefined, true);
    }
})



router.post('/', imageUpload.single('file'), async (req, res) => {
    var csvData = []; let status = [], key = 0;
    let path = "temp/file.csv";
    let db1 = mongo.get().collection("user_table")
    let max = await db1.find({"role": "STUDENT"}).sort({ _id: -1 }).limit(1).toArray()
    if (max.length !== 0) new_id = parseInt(max[0]._id) + 1;
    if (max.length !== 0){
    max = parseInt(max[0]._id);
    max = max + 1;
    }
    else{
        max=101;
    }
    await fs.createReadStream(path)
        .pipe(parse({ delimiter: ',' }))
        .on('data', async function (csvrow) {
            csvData.push(csvrow);
        })
        .on('end', async function () {
            for (let i = 1; i < csvData.length; i++) {
                let new_id = parseInt(max);
                let password = csvData[i][9].trim();

                let json = {
                    "_id": new_id,
                    "key": new_id,
                    "first_name": csvData[i][1].toUpperCase().trim(),
                    "last_name": csvData[i][2].toUpperCase().trim(),
                    "community": csvData[i][3].toUpperCase().trim(),
                    "ph_no": csvData[i][4].trim(),
                    "email": csvData[i][5].trim(),
                    "appliction_number": csvData[i][6].trim(),
                    "tancet_registration_number": csvData[i][7].trim(),
                    "tancet_mark":parseFloat(csvData[i][8]),
                    "password": MD5(password).toString(),
                    "role": "student",
                    "degree": csvData[i][10].toUpperCase().trim(),
                    "status": {
                        "sslc": -2,
                        "hsc": -2,
                        "tc": -2,
                        "tn-marksheet": -2,
                        "tn-hallticket": -2,
                        "cc": -2,
                        "sign": -2,
                        "photo": -2,
                        "sem1": -2,
                        "sem2": -2,
                        "sem3": -2,
                        "sem4": -2,
                        "sem5": -2,
                        "sem6": -2,
                        "add-certificate1": -2,
                        "add-certificate2": -2,
                        "degree": -2
                    }
                }
                var pattern_phno = /^[1-9]\d{9}$/;
                var pattern_email = /\S+@\S+\.\S+/;
                var pattern_appno = /^[0-9]\d{9}$/;
                var pattern_tanregno = /^[0-9]\d{7}$/;
                var pattern_password = /^[A-Za-z]\w{6,14}$/;
                var pattern_tanmark = /^-?\d*(\.\d+)?$/;

                if (!json.first_name === "" || !/^[A-Za-z ]+$/.test(json.first_name)) {
                    status.push({ json: json.first_name, addstatus: 0, msg: "Invalid First Name", "key": ++key });
                }
                else if (!json.last_name === "" || !/^[A-Za-z ]+$/.test(json.last_name)) {
                    status.push({ json: json.last_name, addstatus: 0, msg: "Invalid Last Name", "key": ++key });
                }
                else if (!json.community === "" || !/^[A-Za-z ]+$/.test(json.community)) {
                    status.push({ json: json.community, addstatus: 0, msg: "Invalid Community", "key": ++key });
                }
                else if (!json.ph_no === "" || !pattern_phno.test(json.ph_no)) {
                    status.push({ json: json.ph_no, addstatus: 0, msg: "Invalid Phone  Number", "key": ++key });
                }
                else if (!json.email === "" || !pattern_email.test(json.email)) {
                    status.push({ json: json.email, addstatus: 0, msg: "Invalid Email ID", "key": ++key });
                }
                else if (!json.appliction_number === "" || !pattern_appno.test(json.appliction_number)) {
                    status.push({ json: json.appliction_number, addstatus: 0, msg: "Invalid Application Number", "key": ++key });
                }
                else if (!json.tancet_registration_number === "" || !pattern_tanregno.test(json.tancet_registration_number)) {
                    status.push({ json: json.tancet_registration_number, addstatus: 0, msg: "Invalid Tancet Registration Number", "key": ++key });
                }
                else if (!json.tancet_mark === "" || !pattern_tanmark.test(json.tancet_mark)) {
                    status.push({ json: json.tancet_registration_number, addstatus: 0, msg: "Invalid Tancet mark", "key": ++key });
                }

                else if (!password === "" || !pattern_password.test(password)) {
                    status.push({ json: password, addstatus: 0, msg: "Invalid Password", "key": ++key });
                }
                else if (!json.degree === "" || !/^[A-Za-z. ]+$/.test(json.degree)) {
                    status.push({ json: json.degree, addstatus: 0, msg: "Invalid Degree", "key": ++key });
                }
                else {
                    let user = await db1.insertOne(json);
                    if (user.acknowledged) {
                        max = max + 1;
                        status.push({ json: json.first_name, addstatus: 1, msg: "Successfully Added", "key": ++key });
                    }
                }
            }
            res.json({ Status: 1, addStatus: status });
        });

}, (error, req, res, next) => {
    res.status(400).send({ error: error.message });
});

module.exports = router;