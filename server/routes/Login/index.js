const express = require('express');
const router = express.Router();
const mongo = require('../../mongo');
const MD5 = require('crypto-js/md5');//secure hash funtions Its use for password

router.post('/', async (req, res) => {
    let USERNAME = req.body.USERNAME;
    let PASSWORD = MD5(req.body.PASSWORD).toString();
    let db1 = mongo.get().collection("user_table")
    let loginUser = await db1.find({ "user": USERNAME, "password": PASSWORD }).toArray()
        .catch(err => res.json({ "Status": -1, msg: "Login Error.t Contact Admin!!" }));
    console.log(loginUser);
    if (loginUser.length > 0) {
        res.json({ "Status": 1, user_details: loginUser[0] });
    }
    else {
        res.json({ "Status": 0 });
    }
});


router.post('/forgotpassword', async (req, res) => {
    let USERID = parseFloat(req.body.USERID);
    let EMAIL = req.body.EMAIL;
    let random = Math.random().toString(36).substring(2, 10);

    let db1 = mongo.get().collection("user_table")
    let loginUser = await db1.find({ "_id": USERID }).toArray()
        .catch(err => res.json({ "Status": -1, msg: "Login Error. Contact Admin!!" }));

    if (loginUser[0].email === EMAIL) {

        let query = {
            "password": random

        }

        let newpassword = await db1.updateOne({ "_id": USERID }, { $set: query })
            .catch(err => res.json({ "Status": -1, msg: "Error. " }));
        if (newpassword.acknowledged) {
            const send = require('gmail-send')({
                user: 'blessondanielraj.s@gmail.com',
                pass: '********',
                to: EMAIL,
                subject: 'password',
            });
            send({
                text: random,
            }, (error, result, fullResult) => {
                if (error) console.error(error);

            })
        }
        res.json({ "Status": 1 })
    }
    else {
        res.json({ "Status": 0 })
    }
});

router.post('/checked', async (req, res) => {
    let db1 = mongo.get().collection("user_table")
    let registration = await db1.find({ "role": "admin" }).toArray()
    res.json({ checked: registration[0].registration });
});



module.exports = router;