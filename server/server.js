const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
var cors = require("cors");
const app = express();
const Routes = require("./routes/index");

const mongo = require("./mongo");
mongo.connect(function (err, client) {
    if (err) {
        console.log(err);
    }
    else {
        console.log("Database pg_db connected!");
    }
});

app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use("/pg_db", Routes);
//path for student upload daocuments
app.use('/uploads', express.static('uploads'));
//path for bulk uploading
app.use('/temp', express.static('temp'));

const port = 7001;
app.listen(port, () => console.log(`Server started on port ${port}`));